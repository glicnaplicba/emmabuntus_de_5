#! /bin/bash

if [[ ${LANG} == fr* ]] ; then

    xdg-open https://www.chatons.org/search/by-service?service_type_target_id=148

else

    xdg-open https://www.chatons.org/en/search/by-service?service_type_target_id=148

fi
