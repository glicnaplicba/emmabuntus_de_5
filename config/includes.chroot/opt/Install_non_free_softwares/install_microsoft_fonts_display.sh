#! /bin/bash

# install_microsoft_fonts_display.sh --
#
#   This file permits to install nofree Arial Microsoft Fonts for the Emmabuntüs Equitable Distribs.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

clear

nom_distribution="Emmabuntus Debian Edition 5"

nom_logiciel_affichage="Arial Microsoft"
nom_paquet=install_microsoft_fonts.sh

dir_install_non_free_softwares=/opt/Install_non_free_softwares

delai_fenetre=20


#  chargement des messages
. /opt/Install_non_free_softwares/emmabuntus_messages.sh


if test -z "$message_charge" ; then

    echo "# $nom_logiciel_affichage software installation canceled : Load message KO"

    exit 0

fi



(

echo "# $msg_install $nom_logiciel_affichage, $thank_waiting"


$dir_install_non_free_softwares/$nom_paquet

echo "100" ;

) |
zenity --progress --pulsate \
       --title="$install_title" \
       --text="$install_text" \
       --width=300 \
       --percentage=0 --auto-close --no-cancel
         if [ $? = "1" ]
         then
           zenity --error --timeout=$delai_fenetre \
            --text="$install_cancel"
         else
           echo "# Install Completed"
         fi




