#!/bin/bash

# emmabuntus_ventoy.sh --
#
#   This file permits to launch Ventoy
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

dir=/opt/ventoy

# Teste si le répertoire existe
if test -d ${dir} ; then

    pkexec /usr/bin/emmabuntus_ventoy_exec.sh

    # Détermination de la version
    if [ $(uname -m) = "x86_64" ] ; then
        ${dir}/VentoyGUI.x86_64
    else
        ${dir}/VentoyGUI.i386
    fi


fi
