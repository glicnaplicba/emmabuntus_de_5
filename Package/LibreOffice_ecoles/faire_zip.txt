# Réupérer un fichier :
# user_LO_des_ecoles_portable.zip ou profil_libreoffice_ecoles_2_05.zip
# à la fin de cette page : https://primtux.fr/telecharger-linterface-libreoffice-des-ecoles/
# voir ces adresses :
# https://primtux.fr/bne-loe/user_LO_des_ecoles_portable.zip
# https://primtux.fr/bne-loe/profil_libreoffice_ecoles_2_05.zip

# Méthode pour créer le zip avec les extenstions lirecouleur.5.0.0.oxt et Grammalecte-fr-v1.3.2.oxt

Déziper ce fichier profil_libreoffice_ecoles_X_xx.zip dans ~/.config/libreoffice_ecoles/user en ayant avant renommé ce dossier user en user_ref

Lancer LibreOffice Writer

Dans LO ouvrir le mode standard

Puis Outils -> Gestions des extensions, puis Ajouter et selctionner les extensions lirecouleur et Grammalecte, puis Fermer.

Puis revenir sur le mode 1, et fermer LO.

Réouvrir LO pour voir si les extensions ont été prises en compte et si l'affichage est correct dans le mode 1.

Aller dans ~/.config/libreoffice_ecoles/user  :

cd ~/.config/libreoffice_ecoles/user

rm ecole/nom_rep.ini

zip -r ../user_LO_des_ecoles_portable_mod.zip .











