#! /bin/bash

# emmabuntus_messages.sh --
#
#   This file permits to store message for script of the Emmabuntus Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)


message_validation="$(eval_gettext 'Select the non-free software you want to install ?')"

message_choix="$(eval_gettext 'Choice')"

message_nom_soft="$(eval_gettext 'Names of software or library')"

message_demarrage_codecs="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Would you like to install non-free software on this system ?')\</span>\n\
\n\
\<span color=\'"'red\'"'>$(eval_gettext 'Warning:')\</span> $(eval_gettext 'Packages \042Proprietary codecs\042 you are about to install are not supported')\n\
$(eval_gettext '(by the Debian or Ubuntu developer communities), and may be illegal in some countries.')\n\
\n\
$(eval_gettext 'If you\047re in a country that requires software patents (that is to say neither Belgium nor France nor Switzerland ...),     ')\n\
$(eval_gettext 'however the law allows you to buy addons such as') \<span color=\'${highlight_color}\'>Fluendo\</span>\n\
$(eval_gettext 'instead of the automatic installation of these \042Proprietary codecs\042.')\n\
\n\
$(eval_gettext 'The main reason why these \042Proprietary codecs\042 are not included is just a matter of law.')\n\
$(eval_gettext 'It imposes a fee for each player using these \042Proprietary codecs\042,')\n\
$(eval_gettext 'it is your responsibility to check their usage restrictions in your country before installation.')"


message_info_password="$(eval_gettext 'Please enter your administrator password:')"

message_demarrage_soft_non_libre="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Would you like to install non-free software') (${nom_logiciel_affichage}) $(eval_gettext 'on this system ?')\</span>\n\
\n\
\<span color=\'"'red\'"'>$(eval_gettext 'Note:')\</span> $(eval_gettext 'This non-free software is available in the distribution, to facilitate its access by eveyone,    ') \n\
$(eval_gettext 'pending the arrival of alternatives, so that newbies can quickly start.')\n\
\n\
$(eval_gettext 'We strongly encourage you to use a free alternative.')"


message_demarrage_soft_non_libre_reinstall="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Non-free software') ${nom_logiciel_affichage} $(eval_gettext 'is already installed, do you want to reinstall it?')\</span>\n\
\n\
\<span color=\'"'red\'"'>$(eval_gettext 'Note:')\</span> $(eval_gettext 'This non-free software is available in the distribution, to facilitate its access by eveyone,     ')\n\
$(eval_gettext 'pending the arrival of alternatives, so that newbies can quickly start.')\n\
\n\
$(eval_gettext 'We strongly encourage you to use a free alternative.')"


URL_Framatalk="\<span color=\'${highlight_color}\'>Framatalk\</span>"
URL_Jitsi_Meet="\<span color=\'${highlight_color}\'>Jitsi Meet\</span>"


# Skype

message_demarrage_soft_non_libre_skype="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Would you like to install non-free software') ${nom_logiciel_affichage} $(eval_gettext 'on this system ?')\</span>\n\
\n\
\<span color=\'"'red\'"'>$(eval_gettext 'Note:')\</span> $(eval_gettext 'This non-free software is available in the distribution to facilitate the communication with everybody.    ') \n\
$(eval_gettext 'As free alternatives, we strongly suggest you use Jitsi or') ${URL_Jitsi_Meet}, $(eval_gettext 'which are already included in Emmabuntüs.     ')"


# Steam

message_demarrage_soft_non_libre_steam="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Would you like to install non-free software') ${nom_logiciel_affichage} $(eval_gettext 'on this system ?')\</span>\n\
\n\
\<span color=\'"'red\'"'>$(eval_gettext 'Note:')\</span> $(eval_gettext 'You need an active internet connection to install this software.   ')  "

message_demarrage_remove_language_not_use="
<span color=\"blue\">')Do you want to remove unnecessary languages in the distribution ?')</span>\n\
\n\
$(eval_gettext 'Some languages are not needed in your system')\n\
$(eval_gettext 'removing them will spare you some space,')\n\
$(eval_gettext 'and avoid future updates for these languages.')"

titre_remove_language_not_use="$(eval_gettext 'Removing unnecessary languages')"

message_validation_remove_language_not_use="$(eval_gettext 'Please select the languages that you no longer need')"

message_choix_remove_language_not_use="$(eval_gettext 'Choice')"

message_nom_soft_remove_language_not_use="$(eval_gettext 'Languages installed on your system')"

message_reposer_question_prochain_demarrage_remove_language_not_use="$(eval_gettext 'Do you want the unnecessary languages removal window to popup at next startup ?')"

message_non_internet_install_ko="\n\
\<span color=\'"'red\'"'>$(eval_gettext 'Installation canceled, you need an active internet connection.     ')\</span>"


message_non_internet="\n\
\<span color=\'"'red\'"'>$(eval_gettext 'Warning:')\</span> $(eval_gettext 'To install the Flash Player and Microsoft fonts you need an internet connection.     ')\n\
$(eval_gettext 'Do you still want to continue other non-free software installation?')\n\
\n\
$(eval_gettext 'You can install these software later on by going to the menu Maintenance -> Non-free software')"


# Définition par défaut des messages en Anglais pour les boites de process

msg_repository_for="$(eval_gettext 'Repository for')"
msg_repository_update="$(eval_gettext 'Repository Update')"
msg_install="$(eval_gettext 'Installation')"
msg_remove="$(eval_gettext 'Removing')"
msg_wait_config="$(eval_gettext 'Waiting for the configuration')"
msg_installed="$(eval_gettext 'is already installed')"
msg_installed_internet="$(eval_gettext 'was already installed via internet')"
msg_installed_reinstall="$(eval_gettext 'is already installed, do you want to reinstall it?')"

thank_waiting="$(eval_gettext 'thank you for waiting')"
install_title="$(eval_gettext 'Installation in progress')"
install_text="$(eval_gettext 'Initialization ...')"
install_cancel="$(eval_gettext 'Installation canceled.')"
install_ko="$(eval_gettext 'Installation Failed.')"

msg_adobe_pluggin="$(eval_gettext 'Adobe Flash Player Plugin for Firefox - Warning: You need an internet connection')"
msg_codecs_audio="$(eval_gettext 'Codecs for audio only')"
msg_firmware_non_free="$(eval_gettext 'Enhanced devices (WiFi, Audio, etc.)  support with non-free drivers')"
msg_arial_fonts="$(eval_gettext 'Arial Microsoft Fonts')"
msg_calibri_fonts="$(eval_gettext 'Calibri Microsoft Fonts')"
msg_skype="$(eval_gettext 'Skype (As free alternatives, we strongly suggest you use Jitsi or Jitsi Meet, which are already included in Emmabuntüs)')"


message_demarrage_soft_non_libre_all="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Would you like to install non-free software on this system ?')\</span>\n\
\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Note:')\</span> $(eval_gettext 'You will be able to install these non-free software later on, by going to')\n\
$(eval_gettext 'the application Menu (icon at the top left), then \042Non-Free Software\042.')\n\
\n\
\<span color=\'"'red\'"'>$(eval_gettext 'Warning:')\</span> $(eval_gettext 'The packages \042Proprietary codecs\042 you are about to install are not supported')\n\
$(eval_gettext 'by the Ubuntu or Debian developers communities, and might be illegal in some countries.')\n\
\n\
$(eval_gettext 'The main reason why these \042Proprietary codecs\042 are not included by default is a matter of law.')\n\
$(eval_gettext 'It imposes a fee for each player using these \042Proprietary codecs\042,')\n\
$(eval_gettext 'before installing them, your are responsible to check their usage restrictions in your country.')"


comment_adobe_pluggin="$(eval_gettext 'Allows you to watch videos stored in the Flash format used by video-on-demand platforms')"
comment_codecs_audio="$(eval_gettext 'Allows you to play audio in MP3 format')"
comment_codecs="$(eval_gettext 'Allows you to play protected DVDs')"
comment_arial_fonts="$(eval_gettext 'Allows better compatibility with documents produced by Microsoft Office suite up to the 2003 version')"
comment_calibri_fonts="$(eval_gettext 'Allows better compatibility with documents produced by Microsoft Office suite since the 2007 version')"
comment_skype="$(eval_gettext 'Skype is proprietary software that allows users to make phone or video calls over Internet, as well as screen sharing')"


message_demarrage_soft="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Would you like to install') ${nom_logiciel_affichage} $(eval_gettext 'on this system ?')\</span>     "

message_no_install_live="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'It is not possible to install software') ${nom_logiciel_affichage} $(eval_gettext 'in live mode.')\</span>     "

# Sélection des messages a afficher en fonction de la langue
# pour les boutons Ok en Cancel de Zenity en mode quetion
# voir Issue 27

zenity_ok_label="$(eval_gettext 'Yes')"
zenity_cancel_label="$(eval_gettext 'No')"


message_charge="OK"
